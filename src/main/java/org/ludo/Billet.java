package org.ludo;

import java.util.List;

import static org.ludo.Fonctions.*;

public class Billet{
    // Déclaration des variables
    private String datePaiement ;
    private String dateReservation ;
    private String dateEmission ;
    private Integer numBillet ;
    private Vol vol ;
    private Siege siege;
    private Passager passager;
    public Passager getPassager() {
        return  this.passager;
    }
    public void setPassager(Passager passager) {
        this.passager = passager;
    }



    public Billet() {
        this.dateReservation = nouvelleDate();
        this.datePaiement = nouvelleDate();
        this.dateEmission = nouvelleDate();
        this.numBillet = genNumBillet();
    }
    public Siege getSiege() {
        return siege;
    }


    public void setSiege(Siege siege) {
        this.siege = siege;
    }

    public void setVol(Vol reservationVol) {
        this.vol = reservationVol;
    }
    public Vol getVol() {
        return this.vol;
    }



//    public void acheterBillet(Vol vol, Passager passager){
//        Avion avion = vol.getAvion();
//
//        List<Siege> sieges = avion.getSieges();
//        for (Siege siege : sieges) {
//            if (siege.getPassager() == null) {
//                siege.setPassager();
//                break;
//            }
//
//        }

}

