package org.ludo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Avion {
// Déclaration des variables
    private String type ;
    private String modele;
    private Integer nbPassager;
    private String codeAvion;
    private List<Siege> sieges;
    private List<String> listeRangee = Arrays.asList("A", "B", "C", "D", "E", "F");



// GETTER
    public List<Siege> getSieges() {
        return  this.sieges;
    }
// SETTER




    public Avion(String type, String modeleAvion, Integer nbPassager, String codeAvion) {
        this.type = type;
        this.modele = modeleAvion;
        this.nbPassager = nbPassager;
        this.codeAvion = codeAvion;
        this.sieges = new ArrayList<>();
        for (int i = 0; i< this.listeRangee.size(); i++) {
            for (int j = 1; j <= 10; j++) {
                Siege a = new Siege(this.listeRangee.get(i), j, 2, this);
                this.sieges.add(a);

            }
        }

    }



}
