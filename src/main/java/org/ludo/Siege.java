package org.ludo;

public class Siege {
    private String allee;
    private Integer numRang;
    private Integer classe;
    private Billet billet;
    private Avion avion;



    public Billet getBillet() {
        return billet;
    }
    public void setBillet(Billet billet) {
        this.billet = billet;
    }
    public void setAvion(Avion avion) {
        this.avion = avion;

    }


    public Siege(String allee, Integer numRang, Integer classe, Avion avion) {
        this.allee = allee;
        this.numRang = numRang;
        this.classe = classe;
        this.avion = avion;
    }


}
