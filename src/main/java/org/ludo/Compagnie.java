package org.ludo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Compagnie {
// Déclaration des variables
    private Integer code;
    private String nomCompagnie;
    private String siegeSocial;
    private List<Vol> vols;
    private Integer numVol;
    private Pilote pilote;
    private Avion avion ;

    public Compagnie(Integer code, String nom, String siegeSocial) {
        this.code = code;
        this.nomCompagnie = nom;
        this.siegeSocial = siegeSocial;
        this.numVol = 0;
        this.vols = new ArrayList<>();
    }

    public String toString() {
        return "Compagnie "+this.nomCompagnie;
    }


// Gestion des vols
    public Vol creationVol(Date dateDepart, Date dateArrivee, String heureDepart, String heureArrivee, String villedepart, String villeArrivee, Avion avion){
        this.numVol++;
        Vol vol =new Vol(numVol, dateDepart, dateArrivee, heureDepart, heureArrivee, villedepart, villeArrivee);
        this.vols.add(vol);
        vol.setCompagnie(this);
        vol.setAvion(avion);
        return vol;
    }
    public void afficherVols() {
        for(Vol vol : this.vols) {
            System.out.println(vol);
        }
    }
    public List<Vol> getVolsByVilleDepart(String villeDepart) {
        /**
         *  Retourne la liste de vol par ville de départ
         */
        List<Vol> listeVols = new ArrayList<>();
        for (Vol vol : this.vols) {
            if (Objects.equals(vol.getVilleDepart(), villeDepart)) {
                listeVols.add(vol);
            }
        }
        return listeVols;
    }
    public void annulerVol(Vol vol_annule) {
        /**
         * Annulation d'un vol
          */

        this.vols.remove(vol_annule);
        vol_annule.setCompagnie(null);
    }

    public void embauche(Pilote employe, Compagnie compagnie){
        List<Pilote> listeEmploye = new ArrayList();
        listeEmploye.add(employe);
        this.pilote = employe;
        List<Compagnie> listeCompagnie = new ArrayList();
        listeCompagnie.add(compagnie);
        this.pilote.ajoutCompagnie(compagnie);
    }
    public void setPilote(Pilote pilote) {
        this.pilote = pilote;
    }

    public Billet creationBillet(Vol reservationVol, Passager passager){
        Billet billet = new Billet();
        boolean volDeCompagnie = false;
        // Si le vol existe
        for (Vol vol : this.vols) {
            if (vol == reservationVol) {
                volDeCompagnie = true;
            }
        }
        if (!volDeCompagnie) {
            return null;
        }
        Avion avionAffretePourLeVol = reservationVol.getAvion();
        List<Siege> sieges = avionAffretePourLeVol.getSieges();
        // je récupère la liste des sièges de l'avion associé à ce vol,
        // et je vais regarder s'il y a un siège disponible
        boolean siegeDisponible = false;
        List<Billet> Billets = new ArrayList<>();
        for (int i=0; i<sieges.size(); i++) {
            if (sieges.get(i).getBillet() == null) {
                // dès que je trouve un siège disponible, je l'associe à mon billet
                // je ne laisse pas la possibilité au client de choisir son billet je
                // lui mets le premier siège disponible
                siegeDisponible = true;
                Billet billetReserver;
                billetReserver = new Billet();
                passager.setBillet(billetReserver);
                Billets.add(billetReserver);
                reservationVol.setBillets(Billets);
                sieges.get(i).setBillet(billetReserver);
                billetReserver.setSiege(sieges.get(i));
                billetReserver.setPassager(passager);
                billetReserver.setVol(reservationVol);
                break;
            }else{
                Billets.add(sieges.get(i).getBillet());
            }
        }
        if (siegeDisponible == true) {
            return billet;
        }
        else {
            return null;
        }
    }
    public void changerAvion(Avion avion, Vol vol){
        vol.setAvion(avion);
    }




}
