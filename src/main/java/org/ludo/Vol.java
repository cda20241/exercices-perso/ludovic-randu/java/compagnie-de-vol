package org.ludo;

import java.util.Date;
import java.util.List;

public class Vol {
    // Déclaration des variables
    private Integer numVol;
    private Date dateDepart;
    private Date dateArrivee;
    private String heureDepart;
    private String heureArrivee;
    private String villeDepart;
    private String villeArrivee;
    private Integer retard;
    private Avion avion;
    private Compagnie compagnie;
    private List<Billet> billets;

    // GETTER
    public Integer getNumVol() {
        return numVol;
    }
    public Date getDateDepart() {
        return dateDepart;
    }
    public Date getDateArrivee() {
        return dateArrivee;
    }
    public String getHeureDepart() {
        return heureDepart;
    }
    public String getHeureArrivee() {
        return heureArrivee;
    }
    public String getVilleDepart() {
        return villeDepart;
    }
    public String getVilleArrivee() {
        return villeArrivee;
    }
    public Integer getRetard() {
        return retard;
    }


// SETTER


    public void setBillets(List<Billet> billets) {
        this.billets = billets;
    }

    @Override
    public String toString() {
        return this.numVol + " " + this.villeDepart + " " + this.villeArrivee + " " + this.dateDepart + " " + this.dateArrivee;
    }


    public Vol(Integer numVol, Date dateDepart, Date dateArrivee, String heureDepart, String heureArrivee, String villeDepart, String villeArrivee) {
        this.numVol = numVol;
        this.dateDepart = dateDepart;
        this.dateArrivee = dateArrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.villeDepart = villeDepart;
        this.villeArrivee = villeArrivee;
        int a = 1;
        String b = String.valueOf(a);

    }
    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public void setAvion(Avion avion) {
        this.avion = avion;
    }

    public Avion getAvion() {
        return avion;
    }


}
