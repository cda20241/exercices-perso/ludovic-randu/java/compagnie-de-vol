package org.ludo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.ludo.Fonctions.genNumBillet;


public class Main {
    public static void main(String[] args) {
    // Création des compagnies
         Compagnie compagnie1 = new Compagnie(74589, "ABC", "Paris" );
         Compagnie compagnie2 = new Compagnie(65848, "ZYX", "Geneve" );
    // Création des pilotes
         Pilote pilote1 = new Pilote(22, "Saispas", "Arthur", "Commandant");
    // Embauche du pilote1 par la compagnie1
         compagnie1.embauche(pilote1, compagnie1);

        Avion avion1= new Avion("Boeing", "747", 120, "ds587" );
        Avion avion2= new Avion("Airbus", "A380", 120, "FGGF" );

        Vol vol1 = compagnie1.creationVol(new Date(2024, 01, 17), new Date(2024, 01, 18), "20:50", "9:50", "Paris", "Maroc", avion1);
        Vol vol2 = compagnie1.creationVol(new Date(2024, 01, 27), new Date(2024, 01, 28), "22:30", "8:40", "Maroc", "Paris", avion1);

//        List<Vol> volsParis = compagnie1.getVolsByVilleDepart("Paris");
//        for (Vol vol : volsParis) {
//            compagnie1.annulerVol(vol);
//        }
//        System.out.println("Vols pour Paris annulés")

        Passager passager1 = new Passager("Randu", "Ludovic", "16/05/1983", "0745541168");
        Passager passager2 = new Passager("Paolo", "Bernard", "06/07/1952", "0645541168");



        compagnie1.changerAvion(avion2, vol1);

        compagnie1.creationBillet(vol1, passager1);
        compagnie1.creationBillet(vol1, passager2);
        compagnie1.creationBillet(vol2, passager1);
        compagnie1.creationBillet(vol2, passager2);








    }
}